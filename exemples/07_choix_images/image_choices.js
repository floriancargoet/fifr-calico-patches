const credits = {
  emoji: "🖼",
  name: "Image choices",
  author: "Florian Cargoët",
  version: "1.0",
  description: "Allows choices to be images.",
  licences: {
    self: "2022",
  },
};

const options = {
  // pas d'options
};

Patches.add(
  function () {
    // Reference to the current group of image choice
    let currentGroup;

    Parser.tag("image-choice", (line, tag, value) => {
      // if the image provided isn't a URL,
      if (!value.startsWith("http")) {
        // ensure our file has a file extension
        value = addFileType(
          value,
          story.options.defaultimageformat,
          story.options.defaultimagelocation
        );
      }

      // generate image code
      line.text = `<img src="${value}"/>`;

      // Push a group on the queue if not yet created
      if (!currentGroup) {
        currentGroup = document.createElement("div");
        currentGroup.classList.add("image-choice-group"); // FIXME .choice
        this.queue.push(currentGroup);
      }

      // Move the choice links to the group as they are added to the DOM
      // (We don't move the choice p since Calico expects it to be there)
      this.outerdiv.addEventListener(
        "passage choice element",
        (ev) => {
          const { element } = ev.detail;
          this.queue.onAdded(() => {
            const newP = document.createElement("p");
            //newP.classList.add("choice"); // FIXME
            newP.appendChild(element.firstChild);
            currentGroup.appendChild(newP);
          });
          this.queue.onRemove(() => {
            if (currentGroup) {
                Element.hide(currentGroup);
                currentGroup = null;
              }
          })
        },
        { once: true }
      );
    });

  },
  options,
  credits
);
