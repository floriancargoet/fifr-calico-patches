Le vortex temporel vous recrache : vous êtes maintenant devant [la maison] de votre enfance. À droite, [un chemin] mène, si vous vous rappelez bien, à un minuscule parc avec un toboggan. #useless-tag

* [la maison] Sans réfléchir à l'effet que le vous de 2023 va faire à vos parents de 1992, vous poussez la porte d'entrée. -> maison_1992
* [un chemin] Aussi tenté que vous soyez de revoir vos parents, vous savez que ce n'est pas raisonnable de jouer ainsi avec le temps. Vous contournez la maison par la droite et rejoignez le parc. -> parc_1992


== maison_1992
+ [Recommencer] #restart
-> END

== parc_1992
+ [Recommencer] #restart
-> END
