const credits = {
  emoji: "🗳",
  name: "Choices in text",
  author: "Florian Cargoët",
  version: "1.0",
  description: "Adds syntax to move choices to the main text.",
  licences: {
    self: "2022",
  },
};

const options = {
  // pas d'options
};

Patches.add(function () {
  const story = this;
  // Une expression régulière qui détecte [choiceText:linkText] ou [choiceText]
  const choiceRegExp = RegExp(/\[([^:\]]+)(?:\:([^\]]+))?\]/g);
  // Une map pour stocker les références vers les liens
  let currentPassageChoicesInText = {};

  // On demande à Calico de détecter notre syntaxe
  Parser.pattern(choiceRegExp, (line) => {
    // Et on remplace toutes les occurences par des liens
    line.text = line.text.replaceAll(
      choiceRegExp,
      (str, choiceText, linkText = choiceText) =>
        `<a data-choice="${choiceText}">${linkText}</a>`
    );
    // Calico ne nous donne qu'un objet "line" dans la callback de Parser.pattern(), on n'a pas encore accès au noeud DOM du paragraphe.
    // Pour contourner ça, on demande à être prévenu lors du prochain ajout d'un paragraphe dans la page.
    // Avec {once: true}, on s'assure de n'être appelé que pour un paragraphe, le prochain, c'est à dire celui qui nous intéresse.
    story.outerdiv.addEventListener(
      "passage line element",
      (ev) => {
        const p = ev.detail.element;
        // C'est ici qu'on récupère les références DOM vers les liens.
        const links = Array.from(p.querySelectorAll("[data-choice]"));
        for (const link of links) {
          // On les mémorise via leur 'choiceText' qu'on a rangé dans l'attribut data-choice.
          currentPassageChoicesInText[link.dataset.choice] = link;
        }
      },
      { once: true }
    );
  });

  // Quand un choix est ajouté, on vérifie s'il correspond à un lien.
  // Si oui, on le cache et on active le lien.
  story.outerdiv.addEventListener("passage choice element", (ev) => {
    const { element, choice } = ev.detail;
    const linkInText = currentPassageChoicesInText[choice.text];
    if (linkInText) {
      // Donnons au lien l'apparence d'un choix.
      linkInText.classList.add("choice");
      linkInText.style.cursor = "pointer";
      // Et rendons le cliquable. Au clic, on effectue le choix.
      linkInText.onclick = () => {
        story.choose(choice, linkInText);
      };
      // Et on cache le choix original.
      element.style.display = "none";
      element.delay = 0;
      element.animation = null;
    }
  });

  // Quand un choix est sélectionné, on désactive et on oublie les liens.
  story.outerdiv.addEventListener("passage end", (ev) => {
    for (const linkInText of Object.values(currentPassageChoicesInText)) {
      linkInText.onclick = null;
      linkInText.classList.remove("choice");
      linkInText.style.cursor = "default";
    }
    currentPassageChoicesInText = {};
  });
}, options, credits);
