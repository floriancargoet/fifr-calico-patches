import memorycard from "../../calico/patches/memorycard.js";
import storage from "../../calico/patches/storage.js";

const credits = {
  emoji: "",
  name: "Inky menu",
  author: "Florian Cargoët",
  version: "1.0",
  description: "Restore restart/save/load menu from Inky's template.",
  licences: {
    self: "2022",
  },
};

const options = {
  // pas d'options
};

function hasSave(story) {
  const save = storage.get("user-save", story.options.memorycard_format, story);
  return Boolean(save);
}

Patches.add(
  function () {
    const story = this;
    const restartEl = document.getElementById("restart");
    const saveEl = document.getElementById("save");
    const loadEl = document.getElementById("load");

    restartEl.addEventListener("click", (ev) => {
      // Restart is broken if the story is waiting.
      story.state = Story.states.idle;
      story.restart();
    });

    saveEl.addEventListener("click", function (ev) {
      memorycard.save(story, "user-save"); // different save than autosave
      loadEl.removeAttribute("disabled");
    });

    if (!hasSave(story)) {
      loadEl.setAttribute("disabled", "disabled");
    }

    loadEl.addEventListener("click", function (ev) {
      if (loadEl.getAttribute("disabled")) return;
      story.clear();
      memorycard.load(story, "user-save");
    });
  },
  options,
  credits
);
