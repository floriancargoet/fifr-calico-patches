const credits = {
  emoji: "🎨",
  name: "Body colors",
  author: "Florian Cargoët",
  version: "1.0",
  description:
    "Adds #checkpoint tag allowing autosave only on certain paragraphs.",
  licences: {
    self: "2022",
  },
};

const options = {
  // pas d'options
};


Patches.add(
  function () {
    const story = this;

    // Sauvegarde des couleurs initiales pour les restaurer au #restart.
    const originalColors = {
        color: document.body.style.color,
        backgroundColor: document.body.style.backgroundColor,
    };

    story.outerdiv.addEventListener("story restarting", () => {
        document.body.style.color = originalColors.color;
        document.body.style.backgroundColor = originalColors.backgroundColor;
    });

    Tags.add("background-color", function (story, color) {
      document.body.style.backgroundColor = color;
    });

    Tags.add("body-colors", function (story, colors) {
      const [text, bg] = colors.split("/");
      document.body.style.color = text;
      document.body.style.backgroundColor = bg;
    });
  },
  options,
  credits
);
