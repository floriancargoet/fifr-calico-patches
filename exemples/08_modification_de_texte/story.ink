
Once upon a time [+foo:in Paris] or [+foo:in Berlin]. #useless
 * [+foo:England please] in London
 * [+foo:nowhere]
 * [+foo:divert] -> blip
 * [+foo:divert and text] blip -> blip

 - The end.
    -> END

// [+id:text] => Displays text, replaced when using a [+id] choice
// [+id] => Displays nothing, replaced when using a [+id] choice

== blip
pouet
oh no
-> END
