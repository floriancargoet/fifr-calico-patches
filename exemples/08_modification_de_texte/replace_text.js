const credits = {
  emoji: "📝",
  name: "Replace text",
  author: "Florian Cargoët",
  version: "1.0",
  description: "Choices can replace previous text.",
  licences: {
    self: "2022",
  },
};

const options = {
  // pas d'options
};

Patches.add(
  function () {
    const story = this;

    let isChoicePattern = false;
    story.outerdiv.addEventListener("passage choice", () => isChoicePattern = true)
    story.outerdiv.addEventListener("passage choice element", () => isChoicePattern = false)

    // A regular expression matching [+id:text] or [+id]
    const replaceableSpanRegExp = RegExp(/\[\+([^:\]]+)(?:\:([^\]]+))?\]/g);
    // And one matching +id:choiceText
    const replacingChoiceRegExp = RegExp(/^\+([^:\]]+)(?:\:([^\]]+))$/);

    let currentReplaceableSpans = {};

    // Detect paragraphs which match this pattern
    Parser.pattern(replaceableSpanRegExp, (line) => {
      // Replace every occurences of [+id:text] with a choice link.
      line.text = line.text.replaceAll(
        replaceableSpanRegExp,
        (str, id, text = "") => `<span data-replaceable="${id}">${text}</span>`
      );
      onNextP((p) => {
        // Here we get the real DOM elements.
        const spans = Array.from(p.querySelectorAll("[data-replaceable]"));
        for (const span of spans) {
          // We store references to the spans, indexed by id.
          const id = span.dataset.replaceable;
          if (!(id in currentReplaceableSpans)) {
            currentReplaceableSpans[id] = [];
          }
          currentReplaceableSpans[id].push(span);
        }
      });
    });


    // Quand un choix avec id est détecté, on enlève l'id pour ne pas l'afficher.
    Parser.pattern(replacingChoiceRegExp, (line) => {
      if (isChoicePattern) {
        const match = line.text.match(replacingChoiceRegExp);
        const id = match && match[1];
        if (id) {
          line.text = match[2];
        }
      }
    });

    // When a choice is selected, we move the next paragraph to the span
    story.outerdiv.addEventListener("passage end", (ev) => {
      const { choice } = ev.detail;
      const match = choice.text.match(replacingChoiceRegExp);
      const id = match && match[1];
      if (id) {
        // lookahead for empty choice
        const empty = isChoiceLineEmpty(story, choice);
        const spans = currentReplaceableSpans[id];
        onNextP((p) => {
          story.queue.onAdded(() => {
            p.style.display = "none";
            const text = empty ? "" : p.innerText.trim();
            for (const span of spans) {
              span.innerText = text;
            }
          });
        });
      }
      currentReplaceableSpans = {};
    });

    function onNextP(fn) {
      story.outerdiv.addEventListener(
        "passage line element",
        (ev) => fn(ev.detail.element),
        { once: true }
      );
    }

    function isChoiceLineEmpty(story, choice) {
      const stream = story.ink.PointerAtPath(choice.targetPath).container
        ._content;
      const empty = stream[0].value === "\n";
      return empty;
    }
  },
  options,
  credits
);
