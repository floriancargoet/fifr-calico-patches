const credits = {
  emoji: "🚪",
  name: "Escape",
  author: "Florian Cargoët",
  version: "1.0",
  description: "Binds the escape key to a direct jump to an exit knot.",
  licences: {
    self: "2022",
  },
};

const options = {
  // La touche qui déclenche le jump
  escape_shortcut: "Escape",
};

Patches.add(
  function () {
    const story = this;

    // Une variable pour stocker le nom du noeud vers lequel il faudra aller.
    let exit_knot;
    // On expose une fonction externe à ink.
    // Il faut la déclarer avec : EXTERNAL set_exit_knot(knot)
    // Et l'utiliser avec : ~ set_exit_knot(-> mon_noeud)
    // Ou pour désactiver : ~ set_exit_knot(0)
    ExternalFunctions.add("set_exit_knot", (knot) => {
      if (knot === 0) {
        exit_knot = null;
      } else {
        exit_knot = String(knot); // nom du noeud
      }
    });

    // Quand on appuie sur la touche définie, s'il y a un exit_knot, on y va.
    Shortcuts.add(story.options.escape_shortcut, () => {
      if (exit_knot) {
        story.jumpTo(exit_knot);
      }
    });
  },
  options,
  credits
);
