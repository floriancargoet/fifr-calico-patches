EXTERNAL set_exit_knot(knot)
Vous pouvez quitter une conversation en cours avec la touche Échap/Esc

* [Conversation] Une conversation…
    ~ set_exit_knot(-> partir_brutalement)
    ** longue
    *** et pénible
    **** qui semble ne jamais devoir se terminer
    ***** mais va-t-il se taire ?
    ~ set_exit_knot(-> fuir)
    ****** …
    ******* il cause encore…
    ******** Ça y est, il a fini !
    ~ set_exit_knot(0)
    -> fin_demo


== partir_brutalement
Vous partez au milieu de la conversation, sans un mot.
-> END

== fuir
Vous faites demi-tour et partez en courant, loin de cet ennuyeux personnage.
-> END

== fin_demo
Mais vous n'avez pas testé la touche Échap…
-> END
