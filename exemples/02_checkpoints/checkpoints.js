import memorycard from "../../calico/patches/memorycard.js";

const credits = {
	emoji: "💾",
	name: "Checkpoints",
	author: "Florian Cargoët",
	version: "1.0",
	description: "Adds #checkpoint tag allowing autosave only on certain paragraphs.",
	licences: {
		self: "2022",
	}
}

const options = {
    // pas d'options
}

Patches.add(function () {
  const story = this;
  Tags.add("checkpoint", () => {
    memorycard.save(story);
  });

  story.outerdiv.addEventListener("story restarting", () => {
    memorycard.save(story);
  });

  story.outerdiv.addEventListener("story ready", () => {
    memorycard.load(story);
  });
}, options, credits);
