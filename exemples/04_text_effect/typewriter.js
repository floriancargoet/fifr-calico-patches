
const credits = {
  emoji: "⌨️",
  name: "Typewriter effect",
  author: "Florian Cargoët",
  version: "1.0",
  description: "Adds a typewriter text effect.",
  licences: {
    self: "2022",
    mit: {
      typewriter: "2016-2017 Tameem Safi"
    }
  },
};

const options = {
  // pas d'options
};

Patches.add(
  function () {
    // Rien à faire ici, tout est dans TextAnimation.add
    // On utilise Patches.add() juste pour les crédits.
  },
  options,
  credits
);

TextAnimation.add("typewriter", {
  added(el) {
    createTypewriter(el);
    // We want a sequential animation
    whenPreviousIsRendered(el, () => {
      Element.show(el);
    });
  },
  show(el) {
    el.style.opacity = "";
    executeTypewriter(el, () => {
      Element.rendered(el);
    });
  },
  rendered(el) {},
  hide(el) {
    Element.remove(el);
  },
});

function whenPreviousIsRendered(el, callback) {
  const previousEl = el.previousSibling;
  if (
    previousEl &&
    (previousEl.state === Element.states.added ||
      previousEl.state === Element.states.rendering)
  ) {
    // If the element is not yet rendered, subscribe to onRendered
    Element.addCallback(previousEl, "onRendered", callback);
  } else {
    // Otherwise, call immediately
    callback();
  }
}

function createTypewriter(paragraph) {
  const content = paragraph.firstChild;
  content.originalHTML = content.innerHTML;
  paragraph.typewriter = new Typewriter(content, {
    delay: 30,
  });
}

function executeTypewriter(paragraph, callback) {
  const content = paragraph.firstChild;
  paragraph.typewriter
    .typeString(content.originalHTML)
    .callFunction(() => {
      destroyTypewriter(paragraph);
      callback();
    })
    .start();
}

function destroyTypewriter(paragraph) {
  const content = paragraph.firstChild;
  paragraph.typewriter.stop();
  // Restore HTML
  content.innerHTML = content.originalHTML;
  delete content.originalHTML;
  delete paragraph.typewriter;
}
